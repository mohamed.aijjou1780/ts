enum planetes {
    mercure = 0.2408467,
    venus = 0.61519726,
    terre = 1,
    mars = 1.8808158,
    jupiter = 11.862615,
    saturne = 29.447498,
    uranus = 84.016846, 
    neptune = 164.79132, 
}

function secondes(sec: number) : number
{
    return sec / 31557600;
}

function conversion(p, secondes: number, name)
{
    switch (name) {
        case p[0.2408467]:
            result = p.mercure *  secondes;
            break;
        case p[0.61519726] :
            result = p.venus *  secondes;
            break;
        case p[1]:
            console.log(`
            Sur ${p[1]}, vous avez ${secondes.toFixed(2)} ans.`);
            break;
        case p[1.8808158]:
            result = p.mars *  secondes;
            break;
        case p[11.862615]:
            result = p.jupiter *  secondes;
            break;
        case p[29.447498]:
            result = p.saturne * secondes;
            break;
        case p[84.016846]:
            result = p.uranus * secondes;
            break;
        case p[164.79132]:
            result = p.neptune * secondes;
            break;
        default:
            console.log("Désolé, je n'ai pas pu trouver votre planete ");
            break;
    }
    if(name != "terre")
    {
    console.log(`
            Sur ${name}, vous auriez ${result.toFixed(2)} années terrestres.
            `);    
    }
    
}

let result = Number(prompt("Donnez-moi votre âge en seconde ?"));
result = secondes(result);

console.log(result);

let planete : any;
planete = prompt("Donnez-moi le nom de la planette sur laquelle vous désirez connaître votre âge en années terrestres ?");

conversion(planetes, result, planete.toLowerCase());