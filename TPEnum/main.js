var planetes;
(function (planetes) {
    planetes[planetes["mercure"] = 0.2408467] = "mercure";
    planetes[planetes["venus"] = 0.61519726] = "venus";
    planetes[planetes["terre"] = 1] = "terre";
    planetes[planetes["mars"] = 1.8808158] = "mars";
    planetes[planetes["jupiter"] = 11.862615] = "jupiter";
    planetes[planetes["saturne"] = 29.447498] = "saturne";
    planetes[planetes["uranus"] = 84.016846] = "uranus";
    planetes[planetes["neptune"] = 164.79132] = "neptune";
})(planetes || (planetes = {}));
function secondes(sec) {
    return sec / 31557600;
}
function conversion(p, secondes, name) {
    switch (name) {
        case p[0.2408467]:
            result = p.mercure * secondes;
            break;
        case p[0.61519726]:
            result = p.venus * secondes;
            break;
        case p[1]:
            console.log("\n            Sur ".concat(p[1], ", vous avez ").concat(secondes.toFixed(2), " ans."));
            break;
        case p[1.8808158]:
            result = p.mars * secondes;
            break;
        case p[11.862615]:
            result = p.jupiter * secondes;
            break;
        case p[29.447498]:
            result = p.saturne * secondes;
            break;
        case p[84.016846]:
            result = p.uranus * secondes;
            break;
        case p[164.79132]:
            result = p.neptune * secondes;
            break;
        default:
            console.log("Désolé, je n'ai pas pu trouver votre planete ");
            break;
    }
    if (name != "terre") {
        console.log("\n            Sur ".concat(name, ", vous auriez ").concat(result.toFixed(2), " ann\u00E9es terrestres.\n            "));
    }
}
var result = Number(prompt("Donnez-moi votre âge en seconde ?"));
result = secondes(result);
console.log(result);
var planete;
planete = prompt("Donnez-moi le nom de la planette sur laquelle vous désirez connaître votre âge en années terrestres ?");
conversion(planetes, result, planete.toLowerCase());
