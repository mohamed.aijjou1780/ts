//Ex 1.4
var integer = 6;
var float = 6.66;
var hex = 0xf00d;
var binary = 666;
var octal = 484;
var negZero = -0;
var actualNumber = NaN;
var largestNumber = Number.MAX_VALUE;
var mostBiglyNumber = Infinity;
var members = [
    integer,
    float,
    hex,
    binary,
    octal,
    negZero,
    actualNumber,
    largestNumber,
    mostBiglyNumber
];
members[0] = 12345;
//Ex 1.5
var sequence = Array.from(Array(10).keys());
var animals = ["Pangolin", "Aardvark", "Echidna", "Binturong"];
var stringAndNumbers = [1, "one", 2, "two", 3, "three"];
var allMyArrays = [sequence, animals, stringAndNumbers];
//Ex 1.6
var inventoryItem = ['fidget wibbit', 11];
var nom = inventoryItem[0], qty = inventoryItem[1];
var msg = addInventory(nom, qty);
console.log("Exercice 1.6", msg);
function addInventory(name, quantity) {
    return "Added ".concat(quantity, " ").concat(name, "(s) to inventory");
}
